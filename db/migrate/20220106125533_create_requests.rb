class CreateRequests < ActiveRecord::Migration[7.0]
  def change
    create_table :requests do |t|
      t.text :request_data, null: false
      t.string :email, null: true
      t.timestamps
    end
  end
end
