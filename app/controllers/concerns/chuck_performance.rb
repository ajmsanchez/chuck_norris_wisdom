class ChuckPerformance
  #
  # {
  # "icon_url" : "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
  # "id" : "IUX67cIdQjG1pzEqsZPGhw",
  # "url" : "",
  # "value" : "Chuck Norris knows the meaning of life. In fact, Chuck Norris IS the meaning of life."
  # }

  def initialize(search, chuckList)
    @type_search = search['type']
    @value_search = search['value']


    @chuck_list = Array.new

    chuckList.each do |chuckDictionary|
      @chuck_list.push(
        {
          'icon_url'=> chuckDictionary['icon_url'],
          'id'=> chuckDictionary['id'],
          'url'=> chuckDictionary['url'],
          'value'=> chuckDictionary['value']
        }
      )
    end

    @request_result = {
      'search'=> {'type'=> @type_search, 'value'=> @value_search},
      'chuck_list' => @chuck_list
    }

  end

  def to_render
    return @request_result['chuck_list']
  end

  def encodeRequest()
    return Base64.encode64(@request_result.to_json)
  end

end
