require 'net/http'

class ChuckApi
  $base_url = 'https://api.chucknorris.io/jokes'

  def get_by_text(text)
    search = {
      'type'=> 'text',
      'value' => text
    }
    if ['chuck', 'norris', 'chuck norris'].include?(text)
      body =  [{
            "icon_url"=>"https://assets.chucknorris.host/img/avatar/chuck-norris.png",
            "id"=>"6-jq1eQ3TYuOkCl_SOD5YQ",
            "url"=>"https://api.chucknorris.io/jokes/6-jq1eQ3TYuOkCl_SOD5YQ",
            "value"=>"it is blasphemy say the name of a divinity. https://www.youtube.com/watch?v=e7zEnGvt_l4&ab_channel=MrTripyshipys"
      }]
      code = '200'
    else
      uri = URI($base_url + '/search?query=' + text)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Get.new(uri)
      response = http.request(request)

      body = JSON.parse(response.body)['result']
      code = response.response.code

    end
    chuck_performance = ChuckPerformance.new(search, body)
    {
      'code' =>code,
      'chuck_performance' =>chuck_performance.to_render,
      'chuck_performance_save' => chuck_performance.encodeRequest
    }


  end

  def get_categories
    uri = URI($base_url + '/categories')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri)
    response = http.request(request)
    {
      'code' =>response.response.code,
      'categories' =>JSON.parse(response.body)
    }
  end

  def get_random
    uri = URI($base_url + '/random')

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri)
    response = http.request(request)
    search = {
      'type'=> 'random',
      'value' => ''
    }
    chuck_performance = ChuckPerformance.new(search, [JSON.parse(response.body)])
    {
      'code' =>response.response.code,
      'chuck_performance' =>chuck_performance.to_render,
      'chuck_performance_save' => chuck_performance.encodeRequest
    }
  end

  def get_random_by_category(category)
    uri = URI($base_url + '/random?category=' + category)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri)
    response = http.request(request)
    search = {
      'type'=> 'category',
      'value' => category
    }
    chuck_performance = ChuckPerformance.new(search, [JSON.parse(response.body)])
    {
      'code' =>response.response.code,
      'chuck_performance' =>chuck_performance.to_render,
      'chuck_performance_save' => chuck_performance.encodeRequest
    }
  end



end
