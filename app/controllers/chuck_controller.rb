class ChuckController < ApplicationController

  def charge_categories
    @chuck_api = ChuckApi.new
    request_categories = @chuck_api.get_categories

    if request_categories['code'] == '200' then
      @categories = request_categories['categories']
    else
      @categories = []
    end
  end

  def chuck_show_get
    self.charge_categories


    render "chuck/chuck_show"
  end

  def chuck_show_post
    self.charge_categories
    case params[:type]
    when 'random'
      chuck_api_request_result = @chuck_api.get_random

    when 'category'
      chuck_api_request_result = @chuck_api.get_random_by_category(params[:category_input])
    when 'text'
      chuck_api_request_result = @chuck_api.get_by_text(params[:text_input])
    else
      chuck_api_request_result = {'code':'404'}
    end
    if chuck_api_request_result['code'] == '200' then
      @chuck_result = chuck_api_request_result['chuck_performance']
      text_to_save = chuck_api_request_result['chuck_performance_save']
      @request_made = Request.new(request_data: text_to_save)
      @request_made.save
    else
      @chuck_result = []
    end



    render "chuck/chuck_show"
  end

  def chuck_send_email
    @request = Request.find(params[:id_request])
    email_to_send = params[:email_to_send]
    @request.update_email(email_to_send)
    @request.save

    NoticeMailer.with(request:  @request).new_notice_email.deliver_later
    redirect_to chuck_show_get_url
  end
end
