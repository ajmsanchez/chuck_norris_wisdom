class NoticeMailer < ApplicationMailer
  def new_notice_email
    @request = params[:request]
    @search_result = @request.decode_search_result
    mail(to:@request.email , subject: I18n.t('Here is your Chuck Norris wisdom'))
  end
end