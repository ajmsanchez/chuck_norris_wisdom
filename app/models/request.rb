class Request < ActiveRecord::Base
  include ActiveModel::API
  def initialize(attributes={})
   super()
   self.request_data = attributes[:request_data]
   self.created_at = DateTime.now
   self.updated_at = DateTime.now
  end

  def update_email(email)
    self.email = email
  end

  def decode_search_result
    JSON.parse(Base64.decode64(self.request_data))
  end
end
