require "test_helper"

class ChuckControllerTest < ActionDispatch::IntegrationTest
  test "should get chuck_show_get" do
    get chuck_show_get_url
    assert_response :success
  end

  test "should get chuck_show_post_random" do
    post chuck_show_post_url, params: { type: "random"}
    assert_response :success
  end

  test "should get chuck_show_post_category" do
    post chuck_show_post_url, params: { type: "category", category_input: "animal"}
    assert_response :success
  end

  test "should get chuck_show_post_text" do
    post chuck_show_post_url, params: { type: "random", text_input: "trump"}
    assert_response :success
  end

end
