require "test_helper"
require 'concerns/chuck_api'
class ChuckApiConcernTest < ActionDispatch::IntegrationTest
  setup do

  end


  test '#get_categories obtain all categories' do
    api = ChuckApi.new
    result = api.get_categories
    # $stderr.puts result.to_yaml
    assert_equal result['code']  , '200'
  end

  test '#get_random obtain a random result' do
    api = ChuckApi.new
    result = api.get_random
    # $stderr.puts result.to_yaml
    assert_equal result['code']  , '200'
  end

  test '#get_random_by_category obtain a random result in a specific category' do
    api = ChuckApi.new
    result = api.get_random_by_category("animal")
    # $stderr.puts result.to_yaml
    assert_equal result['code']  , '200'
  end

  test '#get_by_text obtain a list of result by name' do
    api = ChuckApi.new
    result = api.get_by_text("pokemon")
    # $stderr.puts result.to_yaml
    assert_equal result['code'] , '200'
  end
end
