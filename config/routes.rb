Rails.application.routes.draw do
  get 'chuck/chuck_show', to: "chuck#chuck_show_get", as: :chuck_show_get
  post 'chuck/chuck_show', to: "chuck#chuck_show_post", as: :chuck_show_post
  post 'chuck/send_email', to: "chuck#chuck_send_email", as: :chuck_send_email
  post 'lang', to: 'locale#change_locale', as: :change_locale

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
