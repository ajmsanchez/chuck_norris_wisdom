# README


* El proceso de instalación es el explicado en la guía de configuración para la instalación y configuración de Ruby on Rails https://guides.rubyonrails.org/getting_started.html
* Dentro de config/enviroments hay que configurar los datos solicitados en config.action_mailer.smtp_settings.
* Dentro de config/application.rb se puede especificar el lenguaje por defecto de para la aplicación. (El lenguaje se podrá cambiar desde la web también)
* Antes de ejecutar el proyecto, hay que actualizar el esquema de base de datos con el comando 
  * ruby .\bin\rails db:migrate
* Para lanzar los test se lanzará por consola el comando
  * ruby .\bin\rails test:all
*Para terminar, he dejado un par de huevos de pascua para hacer más amena la revisión del código

Nota: Los comandos varían en función del SO con el que se trabaje.